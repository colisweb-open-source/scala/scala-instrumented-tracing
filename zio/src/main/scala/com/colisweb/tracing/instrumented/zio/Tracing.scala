package com.colisweb.tracing.instrumented.zio

import io.opentracing.Tracer
import io.opentracing.Tracer.SpanBuilder
import io.opentracing.util.GlobalTracer
import zio.{Exit, ZIO}

/**
 * Mixin this trait to allow creating Spans for block with returning type ZIO[R, E, A]
 */
trait Tracing {

  private val tracer: Tracer = GlobalTracer.get()

  implicit class WithSpan[-R, +E, +A](computation: => ZIO[R, E, A]) {

    /**
     * Create a new Span starting and ending with the current block. Allow to pass a serviceName, use when this span
     * will be the chid of another span created by a middleware, for example an message consumer
     * @param name
     *   a name for the span
     * @param serviceName
     *   the serviceName to use for this span, it will be propagated to child spans
     */
    def span(name: String, serviceName: String): ZIO[R, E, A] =
      span(name, serviceName, identity[SpanBuilder](_))

    /**
     * Create a new Span starting and ending with the current block. Allow to pass a serviceName, use when this span
     * will be the chid of another span created by a middleware, for example an message consumer
     * @param name
     *   a name for the span
     * @param serviceName
     *   the serviceName to use for this span, it will be propagated to child spans
     * @param customize
     *   modify the Span metadata, by adding tags or baggageItems
     */
    def span(name: String, serviceName: String, customize: SpanBuilder => SpanBuilder): ZIO[R, E, A] =
      span(name, builder => customize(builder.withTag("service", serviceName)))

    /**
     * Create a new root Span that will be the start of a new Trace (it will not be the child of another Span or Trace)
     * @param name
     *   a name for the span
     * @param serviceName
     *   the serviceName to use for this span, it will be propagated to child spans
     */
    def rootSpan(name: String, serviceName: String): ZIO[R, E, A] =
      rootSpan(name, serviceName, identity[SpanBuilder](_))

    /**
     * Create a new root Span that will be the start of a new Trace (it will not be the child of another Span or Trace)
     * @param name
     *   a name for the span
     * @param serviceName
     *   the serviceName to use for this span, it will be propagated to child spans
     * @param customize
     *   modify the Span metadata, by adding tags or baggageItems
     */
    def rootSpan(name: String, serviceName: String, customize: SpanBuilder => SpanBuilder): ZIO[R, E, A] =
      rootSpan(name, builder => customize(builder.withTag("service", serviceName)))

    /**
     * Create a new root Span that will be the start of a new Trace (it will not be the child of another Span or Trace)
     * @param name
     *   a name for the span
     * @param customize
     *   modify the Span metadata, by adding tags or baggageItems
     */
    def rootSpan(name: String, customize: SpanBuilder => SpanBuilder = identity[SpanBuilder](_)): ZIO[R, E, A] =
      span(name, customize.andThen(_.ignoreActiveSpan()))

    /**
     * Create a new Span starting and ending with the current block.
     * @param name
     *   a name for the span
     * @param customize
     *   modify the Span metadata, by adding tags or baggageItems
     */
    def span(name: String, customize: SpanBuilder => SpanBuilder = identity[SpanBuilder](_)): ZIO[R, E, A] = {
      ZIO
        .uninterruptible(
          ZIO.attempt(customize(tracer.buildSpan(name)).start())
        )
        .foldZIO(
          // ignore error creating span and run computation
          _ => computation,
          // ensure the span is finished after the computation
          success =>
            ZIO
              .acquireReleaseExitWith(ZIO.succeed(success))
              .apply[R, E, A] { case (currentSpan, result) =>
                result match {
                  case Exit.Failure(error) => currentSpan.log(error.toString)
                  case _                   => ()
                }
                currentSpan.finish()
                ZIO.unit
              } { currentSpan =>
                tracer.scopeManager().activate(currentSpan)
                computation
              }
        )
    }
  }
}
