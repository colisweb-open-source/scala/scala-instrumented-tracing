import CompileFlags.*
import Dependencies.*
import DependenciesScopesHandler.*
import PublishSettings.noPublishSettings

lazy val scala213               = "2.13.16"
lazy val scala212               = "2.12.20"
lazy val supportedScalaVersions = List(scala213, scala212)

Global / onChangedBuildSource := ReloadOnSourceChanges

ThisBuild / scalaVersion       := scala213
ThisBuild / crossScalaVersions := supportedScalaVersions
ThisBuild / scalacOptions ++= crossScalacOptions(scalaVersion.value)
ThisBuild / parallelExecution  := false

ThisBuild / pushRemoteCacheTo := Some(
  MavenCache("local-cache", baseDirectory.value / sys.env.getOrElse("CACHE_PATH", "sbt-cache"))
)

lazy val tracing = Project(id = "scala-instrumented-tracing", base = file("."))
  .settings(noPublishSettings)
  .aggregate(annotations, tracingForCats, tracingForZio)

lazy val annotations = Project(id = "scala-instrumented-tracing-annotations", base = file("annotations"))

lazy val tracingForCats = Project(id = "scala-instrumented-tracing-cats", base = file("cats"))
  .settings(libraryDependencies ++= compileDependencies(catsEffect, opentracingUtil))

lazy val tracingForZio = Project(id = "scala-instrumented-tracing-zio", base = file("zio"))
  .settings(libraryDependencies ++= compileDependencies(opentracingUtil, zio))
