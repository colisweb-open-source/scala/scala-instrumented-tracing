import sbt._

object Versions {

  final val catsEffect  = "2.3.3"
  final val opentracing = "0.33.0"
  final val zio         = "2.1.16"
}

object Dependencies {
  final val catsEffect      = "org.typelevel" %% "cats-effect"      % Versions.catsEffect
  final val opentracingUtil = "io.opentracing" % "opentracing-util" % Versions.opentracing
  final val zio             = "dev.zio"       %% "zio"              % Versions.zio
}
