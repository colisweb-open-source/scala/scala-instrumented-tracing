package com.colisweb.tracing.instrumented.cats

import cats.effect.{Bracket, ExitCase}
import io.opentracing.Tracer
import io.opentracing.Tracer.SpanBuilder
import io.opentracing.util.GlobalTracer

/**
 * Mixin this trait to allow creating Spans for block with returning type F[A]
 */
trait Tracing[F[_]] {

  private val tracer: Tracer = GlobalTracer.get()

  implicit class WithSpan[A](computation: => F[A]) {

    /**
     * Create a new Span starting and ending with the current block. Allow to pass a serviceName, use when this span
     * will be the chid of another span created by a middleware, for example an message consumer
     * @param name
     *   a name for the span
     * @param serviceName
     *   the serviceName to use for this span, it will be propagated to child spans
     */
    def span(name: String, serviceName: String)(implicit F: Bracket[F, Throwable]): F[A] =
      span(name, serviceName, identity[SpanBuilder](_))

    /**
     * Create a new Span starting and ending with the current block. Allow to pass a serviceName, use when this span
     * will be the chid of another span created by a middleware, for example an message consumer
     * @param name
     *   a name for the span
     * @param serviceName
     *   the serviceName to use for this span, it will be propagated to child spans
     * @param customize
     *   modify the Span metadata, by adding tags or baggageItems
     */
    def span(name: String, serviceName: String, customize: SpanBuilder => SpanBuilder)(implicit
        F: Bracket[F, Throwable]
    ): F[A] =
      span(name, builder => customize(builder.withTag("service", serviceName)))

    /**
     * Create a new root Span that will be the start of a new Trace (it will not be the child of another Span or Trace)
     * @param name
     *   a name for the span
     * @param serviceName
     *   the serviceName to use for this span, it will be propagated to child spans
     */
    def rootSpan(name: String, serviceName: String)(implicit F: Bracket[F, Throwable]): F[A] =
      rootSpan(name, serviceName, identity[SpanBuilder](_))

    /**
     * Create a new root Span that will be the start of a new Trace (it will not be the child of another Span or Trace)
     * @param name
     *   a name for the span
     * @param serviceName
     *   the serviceName to use for this span, it will be propagated to child spans
     * @param customize
     *   modify the Span metadata, by adding tags or baggageItems
     */
    def rootSpan(name: String, serviceName: String, customize: SpanBuilder => SpanBuilder)(implicit
        F: Bracket[F, Throwable]
    ): F[A] =
      rootSpan(name, builder => customize(builder.withTag("service", serviceName)))

    /**
     * Create a new root Span that will be the start of a new Trace (it will not be the child of another Span or Trace)
     * @param name
     *   a name for the span
     * @param customize
     *   modify the Span metadata, by adding tags or baggageItems
     */
    def rootSpan(name: String, customize: SpanBuilder => SpanBuilder = identity[SpanBuilder](_))(implicit
        F: Bracket[F, Throwable]
    ): F[A] =
      span(name, customize.andThen(_.ignoreActiveSpan()))

    /**
     * Create a new Span starting and ending with the current block.
     * @param name
     *   a name for the span
     * @param customize
     *   modify the Span metadata, by adding tags or baggageItems
     */
    def span(name: String, customize: SpanBuilder => SpanBuilder = identity[SpanBuilder](_))(implicit
        F: Bracket[F, Throwable]
    ): F[A] = {
      F.bracketCase(
        F.catchNonFatal(
          customize(tracer.buildSpan(name)).start()
        )
      ) { currentSpan =>
        tracer.scopeManager().activate(currentSpan)
        computation
      } { case (currentSpan, result) =>
        result match {
          case ExitCase.Error(error) => currentSpan.log(error.getMessage)
          case ExitCase.Canceled     => currentSpan.log("Canceled")
          case _                     => ()
        }
        currentSpan.finish()
        F.unit
      }
    }
  }

}
